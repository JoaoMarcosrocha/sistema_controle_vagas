<?php

$user = "root";
$pass = "";
$url = "mysql:host=localhost;dbname=controle_vagas";

try{
    $conn = new PDO($url, $user, $pass);
    $conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $message = "Conexão criada com sucesso!";
}catch(PDOException $e){
    $message = "Falha na conexão -> " . $e->getMessage();
}
echo '<script>console.log("' . $message . '")</script>';