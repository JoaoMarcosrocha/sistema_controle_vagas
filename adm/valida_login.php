<?php

session_start();
if (isset($_POST["acao"])) {
    //importar arquivo de conexão
    require_once "conexao.php";

    //receber usuario e senha
    $usr = $_POST["usuario"];
    $pass = $_POST["senha"];

    //verificar no DB
    $sql = "SELECT * FROM `administradores` WHERE `administradores`.nome = :usr 
    AND `administradores`.senha = :pass";
    $_ = $conn -> prepare($sql);
    $_ -> bindValue(":usr", $usr);
    $_ -> bindValue(":pass", $pass);
    $_ -> execute();

    // redirecionar usuario
    if($_->rowCount() >= 1){
        header("Location: ../dashboard_vagas.php");
    }else{
        header("Location: ../login.html");
    }

}
