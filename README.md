# sistema_controle_vagas

Atividade desenvolvida no curso técnico. Sistema de controle de vagas.

# Requisitos

- Windows 7+
- WAMP Server

# Instalação

## Código Fonte

1. Baixe o código fonte

2. Extraia-o na pasta C:\wamp64\www\

3. Acesse o site através do navegador pelo link <http://localhost/sistema_controle_vagas>

## Criando Banco de Dados

O banco de dados é responsável por armazenar todos os registros que seram gerenciados pelo usuário do sistema. Siga os passos para criá-lo:

1. Acesse o  link: <http://localhost/phpmyadmin>

2. Logue com o usuário ```root``` e sem senha

3. No menu, acesse "SQL", e cole os textos dos seguntes arquivos do código fonte:
    * utils/
        * administradores/
            * create_table.sql
            * generate_data.sql (utilize quantas vezes achar necessário)
        * vagas/
            * create_table.sql
            * generate_data.sql (utilize quantas vezes achar necessário)

# Contribuição
 * Yam Sol Bertamini