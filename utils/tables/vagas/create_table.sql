USE DATABASE `controle_vagas`;
CREATE TABLE IF NOT EXISTS `controle_vagas`.`cargos`(
    `id` INT NOT NULL AUTO_INCREMENT,
    `situacao` ENUM('ativada', 'desativada') NOT NULL,
    `nome` VARCHAR(255) NOT NULL,
    `descricao` VARCHAR(255) NOT NULL,
    `requisitos` VARCHAR(255) NOT NULL,
    `data_abertura` DATE NOT NULL,
    `jornada_trabalho` ENUM('44', '40', 'horista') NOT NULL,
    `cidade` VARCHAR(255) NOT NULL,
    `uf` VARCHAR(2) NOT NULL,
    `salario` FLOAT NOT NULL,
    `created` DATETIME NOT NULL,
    `modified` DATETIME NOT NULL,
    PRIMARY KEY(`id`)
) ENGINE = MyISAM;