<?php
$situacao = $_GET["situacao"];
$id = $_GET["id"];

include_once "../../adm/conexao.php";
$sql = "UPDATE `cargos` SET `situacao`=:situacao WHERE id=:id";
$_ = $conn -> prepare($sql);
$_ -> bindParam(":id",$id);

if($situacao=="ativada"){
    // $_ -> bindParam(":situacao","desativada");
    $_ -> bindValue(":situacao","desativada");
}else if($situacao=="desativada"){
    $_ -> bindValue(":situacao","ativada");
}

$_ -> execute();
header("Location: ../../dashboard_vagas.php");