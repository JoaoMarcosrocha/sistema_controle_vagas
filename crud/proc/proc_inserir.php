<?php

include_once "../../adm/conexao.php";

$situacao = $_POST["situacao"];
$nome = $_POST["nome"];
$descricao = $_POST["descricao"];
$requisitos = $_POST["requisitos"];
$data_abertura = $_POST["data_abertura"];
$jornada_trabalho = $_POST["jornada_trabalho"];
$cidade = $_POST["cidade"];
$uf = $_POST["uf"];
$salario = $_POST["salario"];

$sql = "INSERT INTO `cargos`(`situacao`, `nome`, `descricao`, `requisitos`, `data_abertura`, `jornada_trabalho`, `cidade`, `uf`, `salario`, `created`, `modified`) VALUES(:situacao, :nome, :descricao, :requisitos, :data_abertura, :jornada_trabalho, :cidade, :uf, :salario, NOW(), NOW())";

$_ = $conn->prepare($sql);
$_->bindValue(":situacao", utf8_decode($situacao));
$_->bindValue(":nome", utf8_decode($nome));
$_->bindValue(":descricao", utf8_decode($descricao));
$_->bindValue(":requisitos", utf8_decode($requisitos));
$_->bindValue(":data_abertura", utf8_decode($data_abertura));
$_->bindValue(":jornada_trabalho", utf8_decode($jornada_trabalho));
$_->bindValue(":cidade", utf8_decode($cidade));
$_->bindValue(":uf", utf8_decode($uf));
$_->bindValue(":salario", $salario);
$_->execute();

header("Location: ../../dashboard_vagas.php");
