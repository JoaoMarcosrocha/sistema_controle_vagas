<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inserir Vaga</title>
    <link rel="shortcut icon" href="../img/favicon.png">
    <!-- Boostrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body class="height: 100vh; max-height: 100vh">
    <header class="navbar navbar-dark sticky-top bg-dark shadow">
        <div class="navbar-nav mx-3 ">
            <a class="px-4 btn btn-danger text-light" href="../dashboard_vagas.php">Voltar</a>
        </div>
    </header>
    <div class="form">
        <div class="d-flex align-items-center justify-content-center">
            <h1>Inserir Vaga</h1>
        </div>
        <div class="form-content container">
            <form action="/crud/proc/proc_inserir.php" method="POST">

                <div class="form-group col">
                    <!-- situacao -->
                    <label for="situacao">Situação (ativada,desativada):</label>
                    <input type="text" autofocus name="situacao" class="form-control" id="situacao">
                    <!-- nome -->
                    <label for="nome">Nome:</label>
                    <input type="text" name="nome" class="form-control" id="nome">
                    <!-- descricao -->
                    <label for="descricao">Descrição:</label>
                    <textarea type="text" name="descricao" class="form-control" id="descricao"></textarea>
                    <!-- requisitos -->
                </div>
                <div class="form-group col">
                    <label for="requisitos">Requisitos:</label>
                    <textarea name="requisitos" class="form-control" id="requisitos"></textarea>
                    <!-- data_abertura -->
                    <label for="data_abertura">Data Abertura:</label>
                    <input type="date" name="data_abertura" class="form-control" id="data_abertura">
                    <!-- jornada_trabalho -->
                    <label for="jornada_trabalho">Jornada Trabalho (44, 40, horista):</label>
                    <input type="text" name="jornada_trabalho" class="form-control" id="jornada_trabalho">
                </div>
                <div class="form-group col">
                    <!-- cidade -->
                    <label for="cidade">Cidade:</label>
                    <input type="text" name="cidade" class="form-control" id="cidade">
                    <!-- uf -->
                    <label for="uf">UF:</label>
                    <input type="text" name="uf" class="form-control" id="uf">
                    <!-- salario -->
                    <label for="salario">Salario (R$):</label>
                    <input type="text" name="salario" class="form-control" id="salario">

                </div>
                <div class="d-flex form-group" style="justify-content: center;">
                    <input type="submit" class="btn btn-success" value="Salvar">
                </div>
            </form>
        </div>
    </div>
</body>

</html>