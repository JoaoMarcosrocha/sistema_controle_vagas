<?php

include_once "adm/conexao.php";

$sql = "SELECT * FROM `cargos` WHERE `situacao` = 'ativada' order by `cargos`.nome";
$_ = $conn->prepare($sql);
$_->execute();
?>

<!doctype html>
<html lang="pt-br">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>HOME - Controle de Vagas</title>

  <!-- Favicons -->

  <link rel="canonical" href="https://getbootstrap.com/docs/5.2/examples/dashboard/">

  <link href="css/bootstrap.min.css" rel="stylesheet">

  <link rel="icon" href="img/favicon.png">

  <style>
    footer {
      background-color: green;


    }

    .bd-placeholder-img {
      font-size: 1.125rem;
      text-anchor: middle;
      -webkit-user-select: none;
      -moz-user-select: none;
      user-select: none;
    }

    @media (min-width: 768px) {
      .bd-placeholder-img-lg {
        font-size: 3.5rem;
      }
    }

    .b-example-divider {
      height: 3rem;
      background-color: rgba(0, 0, 0, .1);
      border: solid rgba(0, 0, 0, .15);
      border-width: 1px 0;
      box-shadow: inset 0 .5em 1.5em rgba(0, 0, 0, .1), inset 0 .125em .5em rgba(0, 0, 0, .15);
    }

    .b-example-vr {
      flex-shrink: 0;
      width: 1.5rem;
      height: 100vh;
    }

    .bi {
      vertical-align: -.125em;
      fill: currentColor;
    }

    .nav-scroller {
      position: relative;
      z-index: 2;
      height: 2.75rem;
      overflow-y: hidden;
    }

    .nav-scroller .nav {
      display: flex;
      flex-wrap: nowrap;
      padding-bottom: 1rem;
      margin-top: -1px;
      overflow-x: auto;
      text-align: center;
      white-space: nowrap;
      -webkit-overflow-scrolling: touch;
    }
  </style>


  <!-- Custom styles for this template -->
  <link href="css/dashboard.css" rel="stylesheet">
</head>

<body>

  <header class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
    <div class="navbar-brand col-md-3 col-lg-2 me-0 px-3 fs-6">
      <img src="img/favicon.png" height="30px" alt="logo">
      Home</div>
    <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="navbar-nav mx-3">
      <a class="px-4 btn bg-success text-light" href="login.html">Login</a>
    </div>
    </div>
  </header>

  <main class="ps-sm-auto mx-auto col-lg-10 px-md-4">
    <div class="container">
      <div class="d-flex flex-row align-items-center pt-3">
        <div class="col-1">
          <img src="img/favicon.png" alt="logo">
        </div>
        <div class="col-1-sm">
          <h1>Home</h1>
        </div>
      </div>
    </div>
    </div>
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
      <table class="table table-striped" width="100%" border="2px">
        <thead>
          <tr>
            <th>NOME</th>
            <th>DESCRIÇÃO</th>
            <th>REQUISITOS</th>
            <th>DATA ABERTURA</th>
            <th>JORNADA DE TRABALHO</th>
            <th>CIDADE</th>
            <th>UF</th>
            <th>SALÁRIO</th>
          </tr>
        </thead>
        <tbody>

          <?php
          while ($usr = $_->fetch(PDO::FETCH_ASSOC)) {
          ?>
            <tr>
              <td><?php echo utf8_encode($usr["nome"]); ?></td>
              <td><?php echo utf8_encode($usr["descricao"]); ?></td>
              <td><?php echo utf8_encode($usr["requisitos"]); ?></td>
              <td><?php echo utf8_encode($usr["data_abertura"]); ?></td>
              <td><?php echo utf8_encode($usr["jornada_trabalho"]); ?></td>
              <td><?php echo utf8_encode($usr["cidade"]); ?></td>
              <td><?php echo utf8_encode($usr["uf"]); ?></td>
              <td><?php echo utf8_encode($usr["salario"]); ?></td>
            </tr>
          <?php
          }
          ?>
        </tbody>
      </table>
    </div>
  </main>
  <script src="js/bootstrap.bundle.min.js"></script>

  <script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js" integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js" integrity="sha384-zNy6FEbO50N+Cg5wap8IKA4M/ZnLJgzc6w2NqACZaK0u0FXfOWRRJOnQtpZun8ha" crossorigin="anonymous"></script>
</body>

</html>